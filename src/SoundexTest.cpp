#include "gmock/gmock.h"
class Soundex
{
public:
  std::string encode(const std::string& world ) const
  {
    return "A";
  }
};
TEST(SoundexEncoding, RetainSoleLetterOfOneLetterWord ){
  Soundex soundex;

  auto encoded = soundex.encode("A");

  ASSERT_THAT(encoded, testing::Eq("A"));
}
