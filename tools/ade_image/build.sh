#!/usr/bin/env bash
# Usage:
#   ./build.sh ./Dockerfile
DOCKERFILE=$1

CONTEXT="$(dirname "${BASH_SOURCE[0]}")"

NAMESPACE=
REPO=tdd
ARCH=amd64
TIME=$(date +%Y%m%d_%H%M)

TAG="${REPO}:master"

# Fail on first error.
set -e

docker build --network=host -t ${TAG} -f ${DOCKERFILE} ${CONTEXT}


